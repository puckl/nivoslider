<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion = '1.1';
$aModule = [
    'id'            => 'ecs_nivoslider',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>NivoSlider 4 OXID</i>',
    'description'   => 'Integration des Nivosliders in den OXID eShop!',
    'version'       => '2.0.1',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'support@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend'        => [
    ],
    'blocks' => [
        ['template' => 'widget/promoslider.tpl', 'block' => 'dd_widget_promoslider', 'file' => '/views/blocks/ecspromoslider.tpl'],
    ],
    'settings' => [
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivothe',    'type' => 'select', 'value' => 'default', 'constraints' => 'default|bar|dark|light'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivoeff',    'type' => 'select', 'value' => 'random',  'constraints' => 'random|sliceDown|sliceDownLeft|sliceUp|sliceUpLeft|sliceUpDown|sliceUpDownLeft|fold|fade|slideInRight|slideInLeft|boxRandom|boxRain|boxRainReverse|boxRainGrow|boxRainGrowReverse'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivospeed',  'type' => 'str',    'value' => '500'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivopause',  'type' => 'str',    'value' => '3000'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivoswipe',  'type' => 'bool',   'value' => 'false'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivodirec',  'type' => 'bool',   'value' => 'true'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivocont',   'type' => 'bool',   'value' => 'true'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivothumb',  'type' => 'bool',   'value' => 'true'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivohopau',  'type' => 'bool',   'value' => 'true'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivoprev',   'type' => 'str',    'value' => '&larr;'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivonext',   'type' => 'str',    'value' => '&rarr;'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivocaon',   'type' => 'bool',   'value' => 'true'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivocss',    'type' => 'bool',   'value' => 'false'],
        ['group' => 'ecs_main', 'name' => 'ecs_nivoslider_nivoaktiv',  'type' => 'bool',   'value' => 'true'],
    ],
];
