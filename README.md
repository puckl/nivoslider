## eComStyle.de NivoSlider 4 OXID

## Composer name

ecs/nivoslider

## Shopversionen

OXID eShop CE/PE 4 - 6

## Installation

Kopieren Sie den entpackten Modulordner **NivoSlider** per FTP in Ihren OXID eShop nach source/modules/ecs/.

Es sollte sich dann folgende Ordnerstruktur ergeben:

```
  source
  └── modules
      └── ecs
          └── NivoSlider
```

Loggen Sie sich anschließend in Ihren Shop-Admin ein und aktivieren das neue Modul unter Erweiterungen/Module.
Anschließend tmp leeren.

## Sonderfall Azure-basierendes Theme oder irgend ein anderes Theme, in dem der Slider nicht angezeigt wird

In der Datei '\Application\views\uncoolesTheme\tpl\widget\promoslider.tpl' folgendes ändern:

**Ganz am Anfang einfügen:**

    [{block name="dd_widget_promoslider"}]

**Ganz am Ende einfügen:**

    [{/block}]

(Natürlich nur, falls noch nicht vorhanden)
Anschließend tmp leeren.

## Lizenzinformationen

Please retain this copyright header in all versions of the software

Copyright (C) Josef A. Puckl | eComStyle.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see {http://www.gnu.org/licenses/}.

## Sonstige Wünsche

Sehr gerne sind wir Ihnen bei allen Aufgaben rund um Ihren Onlineshop behilflich!
Fragen Sie bitte jederzeit einfach an: <https://ecomstyle.de/kontakt/>
