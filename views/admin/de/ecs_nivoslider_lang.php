<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sLangName = 'Deutsch';
$aLang = [
    'charset'                                   	           => 'UTF-8',
    'SHOP_MODULE_GROUP_ecs_main'                            => 'Grundeinstellungen',

    'SHOP_MODULE_ecs_nivoslider_nivothe'                    => 'Slidertheme wählen',
    'SHOP_MODULE_ecs_nivoslider_nivoeff'                    => 'Slidereffekt wählen',
    'SHOP_MODULE_ecs_nivoslider_nivospeed'                  => 'Geschwindigkeit des Übergangseffektes',
    'SHOP_MODULE_ecs_nivoslider_nivopause'                  => 'Zeit wie lange jedes Bild angezeigt wird',
    'SHOP_MODULE_ecs_nivoslider_nivoswipe'                  => 'Touch-Swipe (Wischen zum Blättern) aktivieren',
    'SHOP_MODULE_ecs_nivoslider_nivodirec'                  => 'Ob Vor- & Zurücknavigation angezeigt werden soll',
    'SHOP_MODULE_ecs_nivoslider_nivocont'                   => 'Ob eine 1,2,3... Navigation angezeigt werden soll',
    'SHOP_MODULE_ecs_nivoslider_nivothumb'                  => 'Ob Miniaturbilder zur Navigation angezeigt werden sollen',
    'SHOP_MODULE_ecs_nivoslider_nivohopau'                  => 'Ob die Animation angehalten werden soll wenn der Mauszeiger auf einem Bild ist',
    'SHOP_MODULE_ecs_nivoslider_nivoprev'                   => 'Text für die Zurück-Navigation',
    'SHOP_MODULE_ecs_nivoslider_nivonext'                   => 'Text für die Vor-Navigation',
    'SHOP_MODULE_ecs_nivoslider_nivocaon'                   => 'Artikeltitel und Preis anzeigen',
    'SHOP_MODULE_ecs_nivoslider_nivocss'                    => 'CSS selbst im Shoptheme integrieren (nur aktivieren, wenn Sie wissen, was Sie tun)',
    'SHOP_MODULE_ecs_nivoslider_nivoaktiv'                  => 'Slider global ein (check) oder ausschalten (uncheck)',

    'SHOP_MODULE_ecs_nivoslider_nivoeff_random'             => 'random (zufällig)',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceDown'          => 'sliceDown',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceDownLeft'      => 'sliceDownLeft',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceUp'            => 'sliceUp',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceUpLeft'        => 'sliceUpLeft',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceUpDown'        => 'sliceUpDown',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_sliceUpDownLeft'    => 'sliceUpDownLeft',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_fold'               => 'fold',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_fade'               => 'fade',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_slideInRight'       => 'slideInRight',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_slideInLeft'        => 'slideInLeft',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_boxRandom'          => 'boxRandom',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_boxRain'            => 'boxRain',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_boxRainReverse'     => 'boxRainReverse',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_boxRainGrow'        => 'boxRainGrow',
    'SHOP_MODULE_ecs_nivoslider_nivoeff_boxRainGrowReverse' => 'boxRainGrowReverse ',

    'SHOP_MODULE_ecs_nivoslider_nivothe_default'            => 'Default',
    'SHOP_MODULE_ecs_nivoslider_nivothe_bar'                => 'Bar',
    'SHOP_MODULE_ecs_nivoslider_nivothe_dark'               => 'Dark',
    'SHOP_MODULE_ecs_nivoslider_nivothe_light'              => 'Light',

];
