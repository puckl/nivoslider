[{strip}][{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{if !isset($oConfig)}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]
[{if $oConfig->getConfigParam('ecs_nivoslider_nivoaktiv')}]
    [{assign var=oBanners value=$oView->getBanners()}]
    [{if $oBanners|@count}]
        [{oxscript include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/jquery.nivo.slider.pack.js') priority=10}]

        [{* Swipe aktivieren Start *}]
        [{if $oConfig->getConfigParam('ecs_nivoslider_nivoswipe')}]
            [{oxscript include=$oViewConf->getModuleUrl('ecs/NivoSlider','out/jquery.mobile.custom.min.js') priority=9}]
            [{oxscript add="
                $('#slider').on('swiperight', function() {
                    $('a.nivo-nextNav').trigger('click');
                })
                $('#slider').on('swipeleft', function() {
                    $('a.nivo-prevNav').trigger('click');
                })
            "}]
        [{/if}]
        [{* Swipe aktivieren Ende *}]

        [{if !$oConfig->getConfigParam('ecs_nivoslider_nivocss')}]
            [{oxstyle include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/nivo-slider.css')}]
            [{if $oConfig->getConfigParam('ecs_nivoslider_nivothe') eq 'default'}]
                [{oxstyle include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/themes/default/default.css')}]
            [{/if}]
            [{if $oConfig->getConfigParam('ecs_nivoslider_nivothe')  eq 'bar'}]
                [{oxstyle include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/themes/bar/bar.css')}]
            [{/if}]
            [{if $oConfig->getConfigParam('ecs_nivoslider_nivothe') eq 'dark'}]
                [{oxstyle include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/themes/dark/dark.css')}]
            [{/if}]
            [{if $oConfig->getConfigParam('ecs_nivoslider_nivothe') eq 'light'}]
                [{oxstyle include=$oViewConf->getModuleUrl('ecs/NivoSlider','nivo-slider/themes/light/light.css')}]
            [{/if}]
            <style type="text/css">.nivo-caption{text-align:center;}.theme-default .nivo-controlNav{padding: 5px 0 0 0!important;}</style>
        [{/if}]

        [{assign var=effect value=$oConfig->getConfigParam('ecs_nivoslider_nivoeff')  }]
        [{assign var=speed  value=$oConfig->getConfigParam('ecs_nivoslider_nivospeed')}]
        [{assign var=pause  value=$oConfig->getConfigParam('ecs_nivoslider_nivopause')}]
        [{assign var=direct value=$oConfig->getConfigParam('ecs_nivoslider_nivodirec')|replace:"1":"true"}]
        [{if !$direct}][{assign var=direct value='false'}][{/if}]
        [{assign var=contr  value=$oConfig->getConfigParam('ecs_nivoslider_nivocont')|replace:"1":"true" }]
        [{if !$contr}][{assign var=contr value='false'}][{/if}]
        [{assign var=thumb  value=$oConfig->getConfigParam('ecs_nivoslider_nivothumb')|replace:"1":"true"}]
        [{if !$thumb}][{assign var=thumb value='false'}][{/if}]
        [{assign var=hover  value=$oConfig->getConfigParam('ecs_nivoslider_nivohopau')|replace:"1":"true"}]
        [{if !$hover}][{assign var=hover value='false'}][{/if}]
        [{assign var=nprev  value=$oConfig->getConfigParam('ecs_nivoslider_nivoprev')}]
        [{assign var=nnext  value=$oConfig->getConfigParam('ecs_nivoslider_nivonext')}]

        [{oxscript add="
            $(window).load(function() {
                $('#slider').nivoSlider({
                    effect:             '$effect',
                    slices:             15,
                    boxCols:            8,
                    boxRows:            4,
                    animSpeed:          $speed,
                    pauseTime:          $pause,
                    startSlide:         0,
                    directionNav:       $direct,
                    controlNav:         $contr,
                    controlNavThumbs:   $thumb,
                    pauseOnHover:       $hover,
                    manualAdvance:      false,
                    prevText:           '$nprev',
                    nextText:           '$nnext',
                    randomStart:        false
                });
            });
        "}]

        <div id="wrapper">
            <div class="slider-wrapper theme-[{$oConfig->getConfigParam('ecs_nivoslider_nivothe')}]">
                <div id="slider" class="nivoSlider">
                    [{foreach from=$oBanners item=oBanner name=banner}]
                        [{assign var=oArticle    value=$oBanner->getBannerArticle()}]
                        [{assign var=sBannerLink value=$oBanner->getBannerLink()}]
                        [{if $sBannerLink}]
                            <a href="[{$sBannerLink}]">
                        [{/if}]
                        [{assign var=sBannerPictureUrl value=$oBanner->getBannerPictureUrl()}]
                        [{if $sBannerPictureUrl}]
                            [{if $oArticle}]
                                [{assign var="sFrom" value=""}]
                                [{assign var="fPrice" value=$oArticle->getFPrice()}]
                                [{if $oArticle->isParentNotBuyable()}]
                                    [{assign var="fPrice" value=$oArticle->getFVarMinPrice()}]
                                    [{if $oArticle->isRangePrice()}]
                                        [{assign var="sFrom" value="PRICE_FROM"|oxmultilangassign}]
                                    [{/if}]
                                [{/if}]
                                [{assign var="currency" value=$oView->getActCurrency()}]
                            [{/if}]
                            <img src="[{$sBannerPictureUrl}]" [{if $oArticle && $oConfig->getConfigParam('ecs_nivoslider_nivocaon')}]title="[{$oArticle->oxarticles__oxtitle->value}][{oxhasrights ident="SHOWARTICLEPRICE"}]&nbsp;&nbsp;[{$sFrom}] [{$fPrice}] [{$currency->sign}] *[{/oxhasrights}]"[{/if}] alt="[{$oArticle->oxarticles__oxtitle->value}]" data-thumb="[{$sBannerPictureUrl}]">
                        [{/if}]
                        [{if $sBannerLink}]
                            </a>
                        [{/if}]
                    [{/foreach}]
                </div>
            </div>
        </div>

    [{/if}]
[{else}]
    [{$smarty.block.parent}]
[{/if}]
[{/strip}]